# Hacking for Humanity Nov 2020

Somos Beatriz, Nacho, Javier y Alicia (aka equipo2 en discord), y en este repo encontraréis el código y recursos de nuestra propuesta de proyecto para el hackathon: **una web para buscar alternativas de ocio y reuniones sociales segura en la era COVID**.

El problema que creemos que afecta a una gran parte de la población (incluidos nosotros mismos) es la dificultad de encontrar planes de ocio que minimicen riesgos Covid. A pesar de estar en invierno, los planes más buscados consisten en encontrar locales de restauración con terrazas bien ventiladas, que cumplan los protocolos de prevención, etc.. Sin embargo, no hay ninguna herramienta que permita conocer estos detalles con antelación y, además, se une el problema de tener zonas confinadas con distintas regulaciones, zonas con más brotes, zonas con recientes de casos, etc que dificultan aún más definir un plan para varias personas.

Queremos proponer una solución para facilitar este proceso, que ayude de forma visual y sencilla a encontrar locales de ocio seguro teniendo en cuenta las preferencias y localización de los usuarios, las zonas más afectadas por brotes recientes, la movilidad por zonas restringidas, etc..

Además, hacerlo colaborativo para que esté siempre actualizado. Queremos empezar por la ciudad de Madrid, y una vez que la herramienta esté desarrollada, expandirlo a otras comunidades es principalmente un problema de obtención y parseo de datos para cada ciudad o comunidad autónoma, pero la base tecnológica y la herramienta no necesitarían cambios.

Esperamos que os guste :)

## Cómo empezar

El stack tecnológico que hemos usado es el siguiente:
- [Python 3.8](https://www.python.org/downloads/release/python-380/) como lenguaje de programación.
- [Django + Geodjango](https://docs.djangoproject.com/en/3.1/ref/contrib/gis/) para construir la web.
- [Spatialite](https://live.osgeo.org/es/overview/spatialite_overview.html) para la base de datos.
- [Leaflet](https://leafletjs.com/) (javascript) + [Open Street Maps](https://www.openstreetmap.org/#map=12/40.3553/-3.6689) para la visualización de los mapas.
- [Cartociudad](http://www.cartociudad.es/visor/) (del Instituto Geográfico Nacional - Open source) para algunos cálculos puntuales geográficos.

### Pre-requisitos

Disclaimer: Este proyecto sólo ha sido testado sobre los sistemas operativos Ubuntu y MacOS.

- Geodjango utiliza internamente las librerías GEOS, GDAL Y PROJ4, las cuales deben ser instaladas en el SO antes de configurar al proyecto. Toda la información para su instalación aquí: https://docs.djangoproject.com/en/3.1/ref/contrib/gis/install/geolibs
- La base de datos elegida es SpatiaLite, una extensión geográfica de Sqlite, que también hay que instalar tal y como se indica aquí: https://docs.djangoproject.com/en/3.1/ref/contrib/gis/install/spatialite/
- El proyecto utiliza Python 3.8, el cual debe estar instalado en el equipo tal cual se explica en la documentación oficial: https://www.python.org/downloads/release/python-380/

### Instalación del entorno de desarrollo

1. Descargar el código de gitlab.
```
git clone https://gitlab.com/girlsintechspain_/hackathon2020/equipo-2
```
2. Crear y activar el entorno virtual. Puedes hacerlo con los siguientes comandos:
```
$ python3.8 - m venv venv
$ source venv/bin/activate
```
3.   Instalar las librerías del proyecto:
```
$ pip install - r requirements.txt
```
4. Arranca la aplicación django:
```
$ python manage.py runserver
```
5. Abre la web en el navegador: http://localhost:8000

## Estructura del proyecto

A continuación vamos a destacar **algunos** de los ficheros más importantes del proyecto para un mejor entendimiento del codigo:
- `db.sqlite`: base de datos espacial con todos los datos cargados y procesados. En un entorno de producción, sería más seguro y escalable migrarlo a PostGis.
- `hacking_for_humanity_1120/`: carpeta con los ficheros de configuración de django; los ficheros más importantes son los `settings.py`, y la configuración de urls de la api en el fichero `urls.py`.
- safe_leisure/: carpeta con los ficheros de la App de django:
    - `models.py`: fichero con los modelos de la base de datos. En la carpeta `migrations` tenemos el histórico de operaciones que se han hecho en la bd conforme estos modelos han ido cambiando y esos cambios se han trasladado a la bd. Esto asegura la reproducibilidad y trazabilidad de la bd.
    - `views.py`: interfaz y funcionalidad de la api. Toda la lógica geoespacial y de manejo de los datos está en este fichero.
 - `scripts/`: carpeta con ficheros de código que nos han servido para importar/transformar datos.

## Resources
Aquí listamos las fuentes de las cuales hemos obtenido los datos:
- Zonas de salud: https://covid19esrispain-sitesesrispain.hub.arcgis.com/datasets/55e1ad5d44d8473eb8c215d86ed8044a_0
- Incidencia covid por zona de salud: https://datos.comunidad.madrid/catalogo/dataset/covid19_tia_zonas_basicas_salud
- Parques y jardines de la ciudad de Madrid: https://datos.madrid.es/portal/site/egob/menuitem.c05c1f754a33a9fbe4b2e4b284f1a5a0/?vgnextoid=dc758935dde13410VgnVCM2000000c205a0aRCRD&vgnextchannel=374512b9ace9f310VgnVCM100000171f5a0aRCRD&vgnextfmt=default 
- Directorio de cines y teatros: https://datos.madrid.es/portal/site/egob/menuitem.c05c1f754a33a9fbe4b2e4b284f1a5a0/?vgnextoid=842385ce457a8410VgnVCM2000000c205a0aRCRD&vgnextchannel=374512b9ace9f310VgnVCM100000171f5a0aRCRD&vgnextfmt=default
- Censo de locales de restauración y terrazas: https://datos.madrid.es/portal/site/egob/menuitem.c05c1f754a33a9fbe4b2e4b284f1a5a0/?vgnextoid=66665cde99be2410VgnVCM1000000b205a0aRCRD&vgnextchannel=374512b9ace9f310VgnVCM100000171f5a0aRCRD&vgnextfmt=default


## Contribuidoras

* Beatriz Gómez [@beatriz-stylesage](https://github.com/beatriz-stylesage)
* Ignacio Aguado [@ignacioaguado](https://github.com/ignacioaguado)
* Javier Ordoñez [@fjordonez](https://github.com/fjordonez)
* Alicia Pérez [@aliciapj](https://github.com/aliciapj)

## Licencia

Este proyecto utiliza licencia MIT.

## Agradecimientos

* A las organizadoras por su gran labor de difusión y empoderamiento de Girls in Tech.
* A las voluntarias (juezas, mentoras, etc) por su labor altruista y esencial.
* A StyleSage por juntarnos y dejarnos hacer locuras.

