import requests
import json
import logging
import django
from datetime import datetime
import os
import sys
from django.contrib.gis.geos import Polygon

logger = logging.getLogger(__name__)

NUM_ZONAS_SALUD_COMUNIDAD = 285


def setup_script():
    # config log
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hacking_for_humanity_1120.settings")
    parent_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    sys.path.insert(0, parent_dir)
    fmt = '%(asctime)s : %(levelname)s : %(message)s'
    datefmt = '%m%d %H:%M:%S'
    logging.basicConfig(
        level=logging.INFO,
        format=fmt,
        datefmt=datefmt,
    )
    django.setup()


if __name__ == '__main__':

    setup_script()
    logger.info(f"Consultando incidencia zonas basica salud")

    from safe_leisure.models import ZonaBasicaSalud, SanityArea
    from django.contrib.gis.gdal import DataSource
    from django.contrib.gis.geos import Point
    
    zbs_dict = {zbs.id: zbs for zbs in ZonaBasicaSalud.objects.all()}
    
    for sanity_area in SanityArea.objects.all():
        zbs_obj = zbs_dict.get(sanity_area.id)
        
        if not zbs_obj:
            continue
        
        sanity_area.fecha_informe = zbs_obj.fecha_informe
        sanity_area.casos_confirmados_activos_ultimos_14dias = zbs_obj.casos_confirmados_activos_ultimos_14dias
        sanity_area.tasa_incidencia_acumulada_activos_ultimos_14dias = zbs_obj.tasa_incidencia_acumulada_activos_ultimos_14dias
        sanity_area.casos_confirmados_ultimos_14dias = zbs_obj.casos_confirmados_ultimos_14dias
        sanity_area.tasa_incidencia_acumulada_ultimos_14dias = zbs_obj.tasa_incidencia_acumulada_ultimos_14dias
        sanity_area.casos_confirmados_totales = zbs_obj.casos_confirmados_totales
        sanity_area.tasa_incidencia_acumulada_total = zbs_obj.tasa_incidencia_acumulada_total
        sanity_area.codigo_geometria = zbs_obj.codigo_geometria
        sanity_area.confinada = zbs_obj.confinada
        
        sanity_area.save()
        
        print(sanity_area.__dict__)
