import requests
import json
import logging
import django
import os
from datetime import datetime
import os
import sys

logger = logging.getLogger(__name__)

NUM_ZONAS_SALUD_COMUNIDAD = 285


def setup_script():
    # config log
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hacking_for_humanity_1120.settings")
    parent_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    sys.path.insert(0, parent_dir)
    fmt = '%(asctime)s : %(levelname)s : %(message)s'
    datefmt = '%m%d %H:%M:%S'
    logging.basicConfig(
        level=logging.INFO,
        format=fmt,
        datefmt=datefmt,
    )
    parent_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    os.sys.path.insert(0, parent_dir)
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hacking_for_humanity_1120.settings")
    django.setup()
    django.setup()


if __name__ == '__main__':

    setup_script()
    logger.info(f"Consultando incidencia zonas basica salud")

    from safe_leisure.models import ZonaBasicaSalud

    def get_tia_zonas_api():
        url = f'https://datos.comunidad.madrid/api/action/datastore_search?' \
              f'id=43708c23-2b77-48fd-9986-fa97691a2d59&limit={NUM_ZONAS_SALUD_COMUNIDAD}'
        response = requests.request("GET", url)
        records = json.loads(response.text)['result']['records']
        logger.info(f"Se ha obtenido informacion de {len(records)} zonas de salud")
        return records

    def populate_tia_zonas(tia_zona_dict_list):
        counter = 0
        for tia_zona_dict in tia_zona_dict_list:
            cca_14 = int(tia_zona_dict['casos_confirmados_activos_ultimos_14dias']) \
                if tia_zona_dict['casos_confirmados_activos_ultimos_14dias'] != '' else None
            tiaa_14 = float(tia_zona_dict['tasa_incidencia_acumulada_activos_ultimos_14dias'].replace(',', '.')) \
                if tia_zona_dict['tasa_incidencia_acumulada_activos_ultimos_14dias'] != '' else None
            cc_14 = int(tia_zona_dict['casos_confirmados_ultimos_14dias']) \
                if tia_zona_dict['casos_confirmados_ultimos_14dias'] != '' else None
            tia_14 = float(tia_zona_dict['tasa_incidencia_acumulada_ultimos_14dias'].replace(',', '.')) \
                if tia_zona_dict['tasa_incidencia_acumulada_ultimos_14dias'] != '' else None
            cct = int(tia_zona_dict['casos_confirmados_totales']) \
                if tia_zona_dict['casos_confirmados_totales'] != '' else None
            tiat = float(tia_zona_dict['tasa_incidencia_acumulada_total'].replace(',', '.')) \
                if tia_zona_dict['tasa_incidencia_acumulada_total'] != '' else None
            kwargs_get_create = {'id': tia_zona_dict['zona_basica_salud'],
                                 'fecha_informe': datetime.strptime(tia_zona_dict['fecha_informe'],
                                                                    '%Y-%m-%dT%H:%M:%S'),
                                 'casos_confirmados_activos_ultimos_14dias': cca_14,
                                 'tasa_incidencia_acumulada_activos_ultimos_14dias': tiaa_14,
                                 'casos_confirmados_ultimos_14dias': cc_14,
                                 'tasa_incidencia_acumulada_ultimos_14dias': tia_14,
                                 'casos_confirmados_totales': cct,
                                 'tasa_incidencia_acumulada_total': tiat,
                                 'codigo_geometria': int(tia_zona_dict['codigo_geometria'])
                                 }
            obj, created = ZonaBasicaSalud.objects.get_or_create(**kwargs_get_create)
            if obj:
                continue
            if not created:
                logging.error(f"No se ha podido crear la zona {tia_zona_dict['zona_basica_salud']}")
            else:
                counter += 1
        return counter

    num_created = populate_tia_zonas(get_tia_zonas_api())

    logger.info(f"Creadas {num_created} zonas en la BBDD")
