import requests
import json
import logging
import django
import os
import pandas as pd
from datetime import datetime
import math
from tqdm import tqdm

logger = logging.getLogger(__name__)

path_csv = '../data/datos_locales_clean.csv'

def setup_script():
    # config log
    fmt = '%(asctime)s : %(levelname)s : %(message)s'
    datefmt = '%m%d %H:%M:%S'
    logging.basicConfig(
        level=logging.INFO,
        format=fmt,
        datefmt=datefmt,
    )
    parent_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    os.sys.path.insert(0, parent_dir)
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hacking_for_humanity_1120.settings")
    django.setup()
    django.setup()


if __name__ == '__main__':

    setup_script()
    logger.info(f"Leyendo fichero de hosteleria")

    from safe_leisure.models import Local
    from django.contrib.gis.geos import *

    def populate_tia_zonas(local_dict_list):
        counter = 0
        for local_dict in tqdm(local_dict_list):
            aforo_terraza = int(local_dict['aforo_terraza']) if not math.isnan(local_dict['aforo_terraza']) else None
            id_local = int(local_dict['id_local']) if not math.isnan(local_dict['id_local']) else None
            coordenada_x = float(local_dict['coordenada_x'].replace(',', '.'))
            coordenada_y = float(local_dict['coordenada_y'].replace(',', '.'))
            pnt = GEOSGeometry(f'SRID=32630;POINT({coordenada_x} {coordenada_y})')
            pnt.transform(4258)
            latitud = pnt.x
            longitud = pnt.y
            kwargs_get_create = {'aforo_terraza': aforo_terraza,
                                 'coordenada_x': float(local_dict['coordenada_x'].replace(',', '.')),
                                 'coordenada_y': float(local_dict['coordenada_y'].replace(',', '.')),
                                 'latitud': latitud,
                                 'longitud': longitud,
                                 'desc_ubicacion_terraza': str(local_dict['desc_ubicacion_terraza']),
                                 'direccion_local': local_dict['direccion_local'],
                                 'horario_terraza_fin': datetime.strptime(local_dict['horario_terraza_fin'],
                                                                          '%H:%M:%S'),
                                 'horario_terraza_ini': datetime.strptime(local_dict['horario_terraza_ini'],
                                                                          '%H:%M:%S'),
                                 'id_local': id_local,
                                 'id_terraza': int(local_dict['id_terraza']),
                                 'is_terraza': bool(local_dict['is_terraza']),
                                 'nombre': local_dict['nombre'],
                                 'tipo': local_dict['tipo'],
                                 }
            obj, created = Local.objects.get_or_create(**kwargs_get_create)
            if obj:
                continue
            if not created:
                logging.error(f"No se ha podido crear el local {local_dict['nombre']}")
            else:
                counter += 1
        return counter

    df = pd.read_csv(path_csv)
    df['horario_terraza_ini'].fillna("00:00:00", inplace=True)
    df['horario_terraza_fin'].fillna("00:00:00", inplace=True)
    df['id_terraza'].fillna(0, inplace=True)
    local_dict_list = df.to_dict('records')

    num_created = populate_tia_zonas(local_dict_list)
    logger.info(f"Creados {num_created} locales en la BBDD")
