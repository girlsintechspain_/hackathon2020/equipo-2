from pathlib import Path
from django.contrib.gis.utils import LayerMapping
from .models import SanityArea

world_mapping = {
    'id' : 'DS_NOMBRE',
    'mpoly' : 'MULTIPOLYGON',
}

world_shp = 'data/109f161c-4d3d-4aa9-b3b4-1ba0ceb1216e2020315-1-c1v3zk.amd9b.shp'

def run(verbose=True):
    lm = LayerMapping(SanityArea, world_shp, world_mapping, transform=False)
    lm.save(strict=False, verbose=verbose)