from django.contrib.gis import admin
from safe_leisure.models import SanityArea, POI

# Register your models here.

admin.site.register(SanityArea, admin.GeoModelAdmin)
admin.site.register(POI, admin.GeoModelAdmin)