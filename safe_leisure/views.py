from pycartociudad import geocode
from django.shortcuts import render
import requests
import json
from django.db.models import Q

from safe_leisure.models import SanityArea, POI
from django.contrib.gis.geos import Polygon


def get_lat_long_from_route(route):
    res = geocode(route)

    latitude = res['lat']
    longitude = res['lng']

    return latitude, longitude


def location(request):
    try:
        lat = request.GET['lat']
        lng = request.GET['lng']

        distance = request.GET['distance']
        
        parks = True if request.GET['parks'] == 'true' else False
        # restaurants without terrace
        restaurants = True if request.GET['restaurants'] == 'true' else False
        # restaurants with terrace
        terrace = True if request.GET['terrace'] == 'true' else False
        cinema = True if request.GET['cinema'] == 'true' else False
        theater = True if request.GET['theater'] == 'true' else False

        tipo_dict = {'parques_jardines': parks,
                     'restauracion': restaurants,
                     'cines': cinema,
                     'teatros': theater}

        tipo_list = [item for item, value in tipo_dict.items() if value]


        infl_area = service_areas(lng, lat, distance)
        
        converted_infl_area = [[point[1],point[0]] for point in  infl_area['coordinates'][0]]
        
        # TODO include Bea's parameters
        sanity_areas = SanityArea.objects.filter(mpoly__intersects=Polygon(infl_area['coordinates'][0]))
        
        context = {
            "init_lat": lat,
            "init_long": lng,
            "filters": {
                "parks": int(parks),
                "restaurants": int(restaurants),
                "terrace": int(terrace),
                "cinema": int(cinema),
                "theater": int(theater),
                "distance": distance
            },
            "radio_area": converted_infl_area,
            "infl_areas": []
        }

        for sanity_area in sanity_areas:
            if sanity_area.tasa_incidencia_acumulada_ultimos_14dias < 300:
                covid_impact = 1
            elif sanity_area.tasa_incidencia_acumulada_ultimos_14dias < 600:
                covid_impact = 2
            else:
                covid_impact = 3
                
            converted_points = [[point[1], point[0]] for point in [ring for ring in sanity_area.mpoly[0]][0]]
            cut_area = sanity_area.mpoly.intersection(Polygon(infl_area['coordinates'][0]))
            converted_cut_area = [[point[1], point[0]] for point in [ring for ring in cut_area][0]]
            
            area_dict = {
                    "area": converted_points,
                    "cut_area": converted_cut_area,
                    "nombre": sanity_area.id,
                    "covid_impact": covid_impact,
                    "restricted_area": int(sanity_area.confinada),
                    "pois": []
                }

            qs = Q(geom__intersects=sanity_area.mpoly) & Q(geom__intersects=Polygon(infl_area['coordinates'][0]))

            if 'terrace':
                qs = qs & (Q(tipo__in=tipo_list) | (Q(tipo='restauracion') & Q(is_terraza=terrace)))
                pois = POI.objects.filter(qs)
            else:
                pois = POI.objects.filter(qs & Q(tipo__in=tipo_list))

            for poi in pois:
                area_dict['pois'].append(
                    {
                        "lat": poi.latitud,
                        "lng": poi.longitud,
                        "name": poi.nombre,
                        "type": poi.tipo,
                        "address": poi.direccion_local,
                        "features": {
                            "outdoors": poi.desc_ubicacion_terraza,
                            "max_capacity": poi.aforo_terraza or 0,
                            "terrace_times": f'{poi.horario_terraza_ini} - {poi.horario_terraza_fin}'
                        }
                    }
                )
            context['infl_areas'].append(area_dict)

        return render(request, 'location.html', context)

    except Exception as e:
        print(e)
        context = {
            "filters": {
                "parks": 1,
                "restaurants": 1,
                "terrace": 1,
                "cinema": 1,
                "theater": 1,
                "distance": 500
            },
            "radio_area": [],
            "infl_areas": []
        }
        return render(request, 'location.html', context)

def search_by_dir(request):
    lat, lon = get_lat_long_from_route('calle alejandro sanchez 1, madrid')
    context = {'init_lat': lat, 'init_long': lon}
    return render(request, 'index.html', context)


def service_areas(lon: float, lat: float, dist: float):

    # build url
    url = f'http://www.cartociudad.es/services/api/serviceArea?lon={lon}&lat={lat}&dist={dist}'

    # perform request
    r = requests.get(url)

    # format output
    #result = r.text.replace('callback(', '')[:-1]
    result = json.loads(r.text)

    return result or {}
