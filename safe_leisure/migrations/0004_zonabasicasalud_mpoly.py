# Generated by Django 3.1.3 on 2020-11-25 23:51

import django.contrib.gis.db.models.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('safe_leisure', '0003_zonabasicasalud_confinada'),
    ]

    operations = [
        migrations.AddField(
            model_name='zonabasicasalud',
            name='mpoly',
            field=django.contrib.gis.db.models.fields.MultiPolygonField(null=True, srid=4326),
        ),
    ]
