from django.contrib.gis.db import models

class SanityArea(models.Model):
    
    id = models.CharField(primary_key=True, max_length=40)
    fecha_informe = models.DateTimeField(null=True, blank=True, default=None)
    casos_confirmados_activos_ultimos_14dias = models.IntegerField(null=True, blank=True)
    tasa_incidencia_acumulada_activos_ultimos_14dias = models.FloatField(null=True, blank=True)
    casos_confirmados_ultimos_14dias = models.IntegerField(null=True, blank=True)
    tasa_incidencia_acumulada_ultimos_14dias = models.FloatField(null=True, blank=True)
    casos_confirmados_totales = models.IntegerField(null=True, blank=True)
    tasa_incidencia_acumulada_total = models.FloatField(null=True, blank=True)
    codigo_geometria = models.IntegerField(null=True, blank=True)
    confinada = models.BooleanField(default=False)
    # GeoDjango-specific: a geometry field (MultiPolygonField)
    mpoly = models.MultiPolygonField(null=True)


class POI(models.Model):
    aforo_terraza = models.IntegerField(null=True, blank=True)
    coordenada_x = models.FloatField(null=True, blank=True)
    coordenada_y = models.FloatField(null=True, blank=True)
    longitud = models.FloatField(null=True, blank=True)
    latitud = models.FloatField(null=True, blank=True)
    desc_ubicacion_terraza = models.TextField(null=True, blank=True, default=None)
    direccion_local = models.TextField(null=True, blank=True, default=None)
    horario_terraza_fin = models.DateTimeField(null=True, blank=True, default=None)
    horario_terraza_ini = models.DateTimeField(null=True, blank=True, default=None)
    id_local = models.IntegerField(null=True, blank=True)
    id_terraza = models.IntegerField(null=True, blank=True)
    is_terraza = models.BooleanField(default=False)
    nombre = models.TextField(null=True, blank=True, default=None)
    tipo = models.TextField(null=True, blank=True, default=None)
    # GeoDjango-specific: a geometry field (PointField)
    geom = models.PointField(null=True)
